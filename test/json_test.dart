import 'package:flutter_test/flutter_test.dart';

void main() {
  group('Test DateTime', () {
    test('description', () {
      final now = DateTime.now();
      final string = now.toIso8601String();
      final date = DateTime.tryParse(string);
      expect(now, isNotNull, reason: 'now is null');
      expect(date!.compareTo(now), 0, reason: 'date is not equal now');
    });
  });
}
