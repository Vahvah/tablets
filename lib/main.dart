import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:tablets_box/model/language/language.dart';
import 'package:tablets_box/ui/router/root_router.dart';

// We create a "provider", which will store a value (here "Hello world").
// By using a provider, this allows us to mock/override the value exposed.
class Counter extends StateNotifier<int> {
  Counter() : super(0);
  void increment() => state++;
}

final helloWorldProvider = Provider((_) => 'Hello world');
final counterProvider = StateNotifierProvider<Counter, int>((ref) => Counter());

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
//  final dbDir = await path_provider.getApplicationDocumentsDirectory();
  Hive.initFlutter();
  await EasyLocalization.ensureInitialized();
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]).whenComplete(() async {
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarIconBrightness: Brightness.light,
        systemNavigationBarColor: Colors.transparent,
        systemNavigationBarIconBrightness: Brightness.light,
        statusBarBrightness: Brightness.dark,
      ),
    );
  });
  runZonedGuarded(
      () => runApp(
            // For widgets to be able to read providers, we need to wrap the entire
            // application in a "ProviderScope" widget.
            // This is where the state of our providers will be stored.
            const ProviderScope(child: MyApp()),
          ),
      (error, stack) {});
}

// Extend ConsumerWidget instead of StatelessWidget, which is exposed by Riverpod
class MyApp extends ConsumerWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final language = ref.watch(languageProvider);
    return EasyLocalization(
      saveLocale: true,
      startLocale: language.toLocale,
      supportedLocales: Language.values.map((e) => e.toLocale).toList(),
      path: 'assets/translations',
      fallbackLocale: Language.en.toLocale,
      child: ScreenUtilInit(
        designSize: const Size(375, 640),
        minTextAdapt: true,
        builder: (BuildContext context, _) {
          return Builder(
            builder: (context) {
              return MaterialApp.router(
                theme: ThemeData(
                  primarySwatch: Colors.blue,
                ),
                routerDelegate: AppRouter.delegate(),
                routeInformationProvider: AppRouter.routeInfoProvider(),
                routeInformationParser: AppRouter.defaultRouteParser(),
                debugShowCheckedModeBanner: false,
                localizationsDelegates: context.localizationDelegates,
                supportedLocales: context.supportedLocales,
                locale: context.locale,
                localeResolutionCallback: (deviceLocale, supportedLocales) {
                  if (deviceLocale != null) {
                    ref.read(languageProvider.notifier).changeCurrentLanguage(
                        Language.fromLocale(deviceLocale), context);
                  }
                  return language.toLocale;
                },
              );
            },
          );
        },
      ),
    );
  }
}
