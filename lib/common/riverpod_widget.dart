import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

abstract class RiverpodWidget<C extends RiverpodController>
    extends ConsumerWidget {
  RiverpodWidget({required this.builder, super.key}) {
    controller = builder();
    controller.widget = this;
  }
  late final C controller;
  final C Function() builder;
  @override
  Widget build(BuildContext context, WidgetRef ref);
}

abstract class RiverpodController<W extends ConsumerWidget> {
  late final W widget;
}
