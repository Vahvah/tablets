class AppIcons {
  static const String medicKitIcon = 'assets/images/medic_kit.png';
  static const String tabletIcon = 'assets/images/tablet.png';
  static const String pen = 'assets/images/pen.png';
  static const String medicalCross = 'assets/images/cross.png';
  static const String trash = 'assets/images/trash.png';
  static const String schedule = 'assets/images/schedule.png';
//  AssetImage('assets/images/medic_kit.png');

}
