import 'package:flutter/material.dart';

class AppColors {
  static const plainText = Colors.black;
  static const hintText = Colors.grey;
  static const addButton = Colors.green;
  static const background = Colors.white;
  static const checkApply = Colors.green;
  static const editMode = Colors.blue;
  static const applyColor = Colors.green;
  static const cancelColor = Color(0xffe7e8ea);
  static const contrastText = Colors.white;
  static const disabled = Colors.grey;
  static const blue = Colors.blue;
  static final itemShadow = Colors.grey.withOpacity(0.5);
}
