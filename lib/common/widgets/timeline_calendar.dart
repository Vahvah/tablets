import 'package:date_picker_timeline/date_picker_timeline.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tablets_box/common/app_extensions.dart';
import 'package:tablets_box/common/app_text_styles.dart';
import 'package:tablets_box/model/language/language.dart';

class TimelineCalendar extends ConsumerWidget {
  TimelineCalendar(DateTime dateTime, {super.key, this.initialDate}) {
    controller = TimelineCalendarController();
  }
  late final DateTime? initialDate;
  late final TimelineCalendarController controller;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    Language language = ref.watch(languageProvider);
    return Column(
      children: [
        DatePicker(
          height: 50.h,
          monthTextStyle: AppTextStyles.text10,
          dayTextStyle: AppTextStyles.text10,
          dateTextStyle: AppTextStyles.text12,
          controller.getOldestScheduleTime(),
          controller: controller,
          initialSelectedDate: initialDate ?? DateTime.now(),
          locale: language.code,
        ),
      ],
    );
  }
}

class TimelineCalendarController extends DatePickerController {
  late DateTime currentDate;

  String getMonth(DateTime date) {
    final lng = _ref.read(languageProvider.notifier).currentLocal.languageCode;
    return DateFormat(DateFormat.MONTH, lng).format(date);
  }

  DateTime getOldestScheduleTime() {
    return DateTime.now(); //TODO:Stub, should update for oldest schedule
  }

  late final StateNotifierProviderRef _ref;
}
