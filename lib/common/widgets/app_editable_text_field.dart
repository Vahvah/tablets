import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tablets_box/common/app_colors.dart';
import 'package:tablets_box/common/app_extensions.dart';
import 'package:tablets_box/common/app_icons.dart';
import 'package:tablets_box/common/app_text_styles.dart';
import 'package:tablets_box/common/riverpod_widget.dart';

class AppEditableTextField
    extends RiverpodWidget<AppEditableTextFieldController> {
  AppEditableTextField({
    this.textStyle,
    this.hintStyle,
    this.hintText,
    this.textAlign,
    super.key,
    required this.textController,
  }) : super(builder: () => AppEditableTextFieldController());
  final TextEditingController textController;
  final TextStyle? textStyle;
  final TextStyle? hintStyle;
  final String? hintText;
  final TextAlign? textAlign;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final editable = ref.watch(controller.editStateProvider);
    return editable
        ? _EditableField(
            textAlign: textAlign ?? TextAlign.start,
            textController: textController,
            controller: controller,
            textStyle: textStyle ??
                AppTextStyles.text16
                    .andColor(AppColors.plainText)
                    .andWeight(FontWeight.bold),
            hintText: hintText ?? '',
            hintStyle: hintStyle ??
                AppTextStyles.text12
                    .andColor(AppColors.hintText)
                    .andWeight(FontWeight.w300),
          )
        : _NotEditableField(
            textAlign: textAlign ?? TextAlign.start,
            textController: textController,
            controller: controller,
            textStyle: textStyle ??
                AppTextStyles.text16
                    .andColor(AppColors.plainText)
                    .andWeight(FontWeight.bold),
            hintText: hintText ?? '',
            hintStyle: hintStyle ??
                AppTextStyles.text12
                    .andColor(AppColors.hintText)
                    .andWeight(FontWeight.w300),
          );
  }
}

class _NotEditableField extends ConsumerWidget {
  const _NotEditableField({
    required this.textController,
    required this.controller,
    required this.textStyle,
    required this.hintStyle,
    required this.hintText,
    required this.textAlign,
  });
  final String hintText;
  final TextStyle textStyle;
  final TextStyle hintStyle;
  final TextEditingController textController;
  final AppEditableTextFieldController controller;
  final TextAlign textAlign;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Column(
      children: [
        Container(
          height: 25.h,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10.r)),
              border: Border.all(color: Colors.grey)),
//          margin: EdgeInsets.all(5.r),
          // padding: EdgeInsets.all(5.r),
          child: Row(
            children: [
              SizedBox(
                  width: screenWidth - 40.w,
                  child: TextField(
                    textAlign: textAlign,
                    decoration: InputDecoration.collapsed(
                      hintStyle: hintStyle,
                      hintText: hintText,
                      enabled: false,
                    ),
                    maxLines: 1,
                    controller: textController,
                    enabled: false,
                    style: textStyle,
                  )),
              GestureDetector(
                onTap: () => controller.onPenTap(ref),
                child: Image.asset(
                  AppIcons.pen,
                  height: 15.h,
                  color: AppColors.editMode,
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}

class _EditableField extends ConsumerWidget {
  const _EditableField({
    required this.textController,
    required this.controller,
    required this.textStyle,
    required this.hintStyle,
    required this.hintText,
    required this.textAlign,
  });
  final String hintText;
  final TextStyle textStyle;
  final TextStyle hintStyle;
  final TextEditingController textController;
  final AppEditableTextFieldController controller;
  final TextAlign textAlign;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Column(
      children: [
        Container(
          height: 25.h,
          decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    blurRadius: 5,
                    offset: const Offset(0, 3),
                    spreadRadius: 2)
              ],
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10.r)),
              border: Border.all(color: Colors.grey)),
//          margin: EdgeInsets.all(5.r),
          // padding: EdgeInsets.all(5.r),
          child: Row(
            children: [
              SizedBox(
                  width: screenWidth - 40.w,
                  child: TextField(
                    autofocus: true,
                    textAlign: textAlign,
                    decoration: InputDecoration.collapsed(
                      hintStyle: hintStyle,
                      hintText: hintText,
                      enabled: true,
                    ),
                    maxLines: 1,
                    controller: textController,
                    enabled: true,
                    style: textStyle,
                  )),
              GestureDetector(
                onTap: () => controller.onCheckTap(ref),
                child: Icon(
                  Icons.check_outlined,
                  size: 15.h,
                  color: AppColors.checkApply,
                ),
              ) //Image.asset(AppIcons.pen, height: 15.h,),)
            ],
          ),
        ),
      ],
    );
  }
}

class AppEditableTextFieldController
    extends RiverpodController<AppEditableTextField> {
  late final StateNotifierProvider editStateProvider;

  AppEditableTextFieldController({bool editableInInit = false}) {
    editStateProvider = StateNotifierProvider<EditStateNotifier, bool>(
        (ref) => EditStateNotifier(editableInInit));
  }
  void onPenTap(WidgetRef ref) {
    final notifier = ref.read(editStateProvider.notifier);
    notifier as EditStateNotifier;
    notifier.setEditableState();
  }

  void onCheckTap(WidgetRef ref) {
    final notifier = ref.read(editStateProvider.notifier);
    notifier as EditStateNotifier;
    notifier.setNotEditableState();
  }
}

class EditStateNotifier extends StateNotifier<bool> {
  EditStateNotifier(bool initState) : super(initState);
  void changeState() {
    state = !state;
  }

  void setEditableState() {
    state = true;
  }

  void setNotEditableState() {
    state = false;
  }
}
