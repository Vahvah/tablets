import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'globals.dart';

final _su = ScreenUtil();

double? get pixelRatio => ScreenUtil().pixelRatio;

double get screenWidth => ScreenUtil().screenWidth;

double? get scaleWidth => ScreenUtil().scaleWidth;

double get screenHeight => ScreenUtil().screenHeight;

double? get scaleHeight => ScreenUtil().scaleHeight;

extension NumX on num {
  double get r => Globals.isAdopt ? _su.radius(this) : toDouble();

  double get sp => Globals.isAdopt ? _su.setSp(this) : toDouble();

  double get w => Globals.isAdopt ? _su.setWidth(this) : toDouble();

  double get w0 => toDouble();

  double get h => Globals.isAdopt ? _su.setHeight(this) : toDouble();

  double get h0 => toDouble();

  SizedBox get sbHeight =>
      SizedBox(height: Globals.isAdopt ? _su.setHeight(this) : toDouble());

  SizedBox get sbHeightFromWidth =>
      SizedBox(height: Globals.isAdopt ? _su.setWidth(this) : toDouble());

  /// Это все равно что по минимальному коэффициенту
  SizedBox get sbHeightFromRadius =>
      SizedBox(height: Globals.isAdopt ? _su.radius(this) : toDouble());

  SizedBox get sbHeight0 => SizedBox(height: toDouble());

  SizedBox get sbWidth =>
      SizedBox(width: Globals.isAdopt ? _su.setWidth(this) : toDouble());

  SizedBox get sbWidthFromHeight =>
      SizedBox(width: Globals.isAdopt ? _su.setHeight(this) : toDouble());

  /// Это все равно что по минимальному коэффициенту
  SizedBox get sbWidthFromRadius =>
      SizedBox(width: Globals.isAdopt ? _su.radius(this) : toDouble());

  SizedBox get sbWidth0 => SizedBox(width: toDouble());

  EdgeInsets get insetsHor => EdgeInsets.symmetric(horizontal: this * 1.0);

  EdgeInsets get insetsVert => EdgeInsets.symmetric(vertical: this * 1.0);

  EdgeInsets get insetsAll => EdgeInsets.all(this * 1.0);
}
