const bool _isAdopt = true;

class Globals {
  static get isAdopt => _isAdopt;
}

///Hive declarations
const String hiveTabletBox = 'tablets';
const int hiveTypeMedicineForm = 0;
const int hiveTypeMedicine = 1;
