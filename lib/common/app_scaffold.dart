import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tablets_box/common/app_extensions.dart';
import 'package:tablets_box/common/app_icons.dart';
import 'package:tablets_box/common/riverpod_widget.dart';
import 'package:tablets_box/ui/screens/medical_kit_screen/medical_kit_screen.dart';
import 'package:tablets_box/ui/screens/tablets_screen/schedule_screen.dart';

class AppScafold extends RiverpodWidget<AppScaffoldController> {
  AppScafold({super.key}) : super(builder: () => AppScaffoldController());

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: _appBar(),
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        body: TabBarView(
          children: [ScheduleScreen(), MedicalKitScreen()],
        ),
        //ignore:sized_box_for_whitespace
        bottomNavigationBar: Container(
          height: 70.h,
          child: Column(
            children: [
              TabBar(
                tabs: [
                  Tab(
                    icon: Image.asset(AppIcons.schedule),
                  ),
                  Tab(
                    icon: Image.asset(AppIcons.medicKitIcon),
                  ),
                ],
              ),
              const Spacer()
            ],
          ),
        ),
      ),
    );
  }
}

PreferredSizeWidget _appBar() {
  return AppBar(
    automaticallyImplyLeading: true,
    elevation: 0,
    toolbarHeight: 0,
    backgroundColor: Colors.white,
    flexibleSpace: Container(),
  );
}

class AppScaffoldController extends RiverpodController<AppScafold> {}
