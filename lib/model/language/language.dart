import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

enum Language implements Comparable<Language> {
  ru(code: 'ru', countryCode: 'RU', name: 'Русский'),
  en(code: 'en', countryCode: 'EN', name: 'English');

  const Language(
      {required this.countryCode, required this.code, required this.name});
  final String countryCode;
  final String code;
  final String name;

  @override
  int compareTo(Language other) {
    if (other == this) {
      return 0;
    }
    if (code.compareTo(other.code) != 0) {
      return code.compareTo(other.code);
    }
    return countryCode.compareTo(other.countryCode);
  }

  static Language fromLocale(Locale locale) {
    for (Language l in Language.values) {
      if (l.code.compareTo(locale.languageCode) == 0) {
        return l;
      }
    }
    return Language.en;
  }

  Locale get toLocale => Locale(code);
}

class LanguageNotifier extends StateNotifier<Language> {
  LanguageNotifier(super.state);
  Locale get currentLocal => state.toLocale;
  Language get currentLanguage => state;
  void changeCurrentLanguage(Language newLanguage, BuildContext context) {
    EasyLocalization.of(context)?.setLocale(newLanguage.toLocale);
    state = newLanguage;
  }
}

final languageProvider =
    StateNotifierProvider<LanguageNotifier, Language>((ref) {
  final locale = WidgetsBinding.instance.platformDispatcher.locale;
  final language = Language.fromLocale(locale);
  return LanguageNotifier(language);
});
