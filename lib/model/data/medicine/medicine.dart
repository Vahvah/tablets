import 'package:easy_localization/easy_localization.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:tablets_box/common/app_icons.dart';

part 'medicine.freezed.dart';
part 'medicine.g.dart';

enum MedicineForm {
  tablet('general.tablet', AppIcons.tabletIcon),
  capsule('general.capsule', AppIcons.tabletIcon),
  syringePen('general.syringePen', AppIcons.tabletIcon),
  drops('general.drops', AppIcons.tabletIcon),
  aerosol('general.aerosol', AppIcons.tabletIcon),
  syrup('general.syrup', AppIcons.tabletIcon);

  const MedicineForm(this._localizationPath, this.defaultIconPath);
  final String _localizationPath;
  final String defaultIconPath;
  String get getLocalizedName => _localizationPath.tr();
}

@freezed
class Medicine with _$Medicine {
  const factory Medicine(
      {required String name,
      required String id,
      required bool scheduled,
      required String description,
      required bool isInKit,
      required MedicineForm form}) = _Medicine;
  factory Medicine.fromJson(Map<String, dynamic> json) =>
      _$MedicineFromJson(json);
}
