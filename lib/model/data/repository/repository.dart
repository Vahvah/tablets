import 'package:tablets_box/model/data/medicine/medicine.dart';
import 'package:tablets_box/model/data/repository/local_repositary/hive_repository.dart';
import 'package:tablets_box/model/data/repository/local_repositary/local_repository.dart';
import 'package:tablets_box/safe_coding/safe_coding.dart';

class _Repository {
  final LocalRepository _local = HiveRepository();
  Future<List<Medicine>> getMedicines() async => await _local.getMedicines();
  Future<void> updateMedicine(Medicine medicine) async =>
      await _local.updateMedicine(medicine);
  Future<void> addMedicine(Medicine medicine) async =>
      await _local.addMedicine(medicine);
  Future<void> deleteMedicine(Medicine medicine) async =>
      await _local.deleteMedicine(medicine);
  Future<Either<AppError, Medicine>> getMedicine(String id) =>
      _local.getMedicine(id);
}

//ignore:non_constant_identifier_names
final Repository = _Repository();
