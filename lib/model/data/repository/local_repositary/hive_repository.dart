import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:tablets_box/common/globals.dart';
import 'package:tablets_box/model/data/medicine/medicine.dart';
import 'package:tablets_box/model/data/repository/local_repositary/local_repository.dart';
import 'package:tablets_box/safe_coding/src/either.dart';
import 'package:tablets_box/safe_coding/src/app_error.dart';

class HiveRepository implements LocalRepository {
  HiveRepository() {
    initFunc = _init().then<bool>((_) => _initialized = true);
  }
  late final Future<bool> initFunc;

  bool _initialized = false;
  Future _init() async {
    debugPrint('HiveRepository._init');
//    await Hive.deleteBoxFromDisk(hiveTabletBox);
    tabletsBox = await Hive.openBox<String>(hiveTabletBox);
    debugPrint('HiveRepository._init box opened');
  }

  late final Box<String> tabletsBox;

  @override
  bool get isInitialized => _initialized;

  @override
  Future<void> init() async {
    if (isInitialized) {
      return;
    }
    await Future.wait(<Future>[initFunc]);
  }

  @override
  Future<void> addMedicine(Medicine medicine) async {
    await init();
    final jsonString = jsonEncode(medicine.toJson());
    await tabletsBox.put(medicine.id, jsonString);
  }

  @override
  Future<void> deleteMedicine(Medicine medicine) async {
    await init();
    await tabletsBox.delete(medicine.id);
  }

  @override
  Future<List<Medicine>> getMedicines() async {
    await init();
    final values = tabletsBox.values;
    return values.map((e) => Medicine.fromJson(jsonDecode(e))).toList();
  }

  @override
  Future<void> updateMedicine(Medicine medicine) async {
    await init();
    final jsonString = jsonEncode(medicine.toJson());
    await tabletsBox.put(medicine.id, jsonString);
  }

  @override
  Future<Either<AppError, Medicine>> getMedicine(String id) async {
    await init();
    final jsonString = tabletsBox.get(id);
    if (jsonString == null) {
      return left(AppError.notFound());
    }
    try {
      return right(Medicine.fromJson(jsonDecode(jsonString)));
    } on Exception catch (e) {
      return left(AppError.exception(exception: e));
    }
  }
}
