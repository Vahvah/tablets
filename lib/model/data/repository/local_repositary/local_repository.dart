import 'package:tablets_box/model/data/medicine/medicine.dart';
import 'package:tablets_box/safe_coding/safe_coding.dart';

abstract class LocalRepository {
  bool get isInitialized;
  void init();
  Future<List<Medicine>> getMedicines();
  Future<void> updateMedicine(Medicine medicine);
  Future<void> addMedicine(Medicine medicine);
  Future<void> deleteMedicine(Medicine medicine);
  Future<Either<AppError, Medicine>> getMedicine(String id);
}
