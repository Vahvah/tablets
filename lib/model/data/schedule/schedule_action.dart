import 'package:freezed_annotation/freezed_annotation.dart';

part 'schedule_action.freezed.dart';
part 'schedule_action.g.dart';

enum ScheduleActionType {
  singleAction,
  courseAction,
  permanentAction,
}

@freezed
class ScheduledAction with _$ScheduledAction {
  factory ScheduledAction.singleAction({
    required String id,
    @Default(ScheduleActionType.singleAction) ScheduleActionType type,
    required String medicineId,
    required int count,
    @DateTimeJsonConverter() required DateTime actionTime,
  }) = _ScheduledSingleAction;

  factory ScheduledAction.courseAction({
    required String id,
    @Default(ScheduleActionType.courseAction) ScheduleActionType type,
    required String medicineId,
    required int count,
    @DateTimeJsonConverter() required DateTime beginCourseDateTime,
    @DateTimeJsonConverter() required DateTime endCourseDateTime,
    @DateTimeJsonConverter() required List<DateTime> actionsTimeList,
  }) = _ScheduledCourseAction;

  factory ScheduledAction.permanentAction({
    required String id,
    @Default(ScheduleActionType.permanentAction) ScheduleActionType type,
    required String medicineId,
    required int count,
    @DateTimeJsonConverter() required DateTime beginActionDateTime,
    @DateTimeJsonConverter() required List<DateTime> actionsTimeList,
  }) = _ScheduledPermanentAction;

  factory ScheduledAction.fromJson(Map<String, dynamic> json) =>
      _$ScheduledActionFromJson(json);
}

class DateTimeJsonConverter extends JsonConverter<DateTime, String> {
  const DateTimeJsonConverter();
  @override
  DateTime fromJson(String json) {
    return DateTime.tryParse(json) ?? DateTime.now();
  }

  @override
  String toJson(DateTime object) {
    return object.toIso8601String();
  }
}
