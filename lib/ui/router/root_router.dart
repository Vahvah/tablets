import 'package:auto_route/auto_route.dart';
import 'package:tablets_box/ui/screens/main_screen.dart';
import 'package:tablets_box/ui/screens/medical_kit_screen/screens/edit_medicine_screen.dart';
import 'package:tablets_box/ui/screens/splash_screen/splash_screen.dart';

import 'root_router.gr.dart';

@MaterialAutoRouter(replaceInRouteName: 'Screen,Route', routes: <AutoRoute>[
  AutoRoute(page: SplashScreen),
  AutoRoute(page: MainScreen, initial: true),
  AutoRoute(page: EditMedicineScreen),
]
    // replaceInRouteName: 'Screen|Dialog,Route',
    // routes: <AutoRoute>[
    //   RedirectRoute(path: '*', redirectTo: '/'),
    //   CustomRoute(page: TabletsScreen, initial: true),
    //   CustomRoute(
    //     page: SplashScreen,
    //     transitionsBuilder: TransitionsBuilders.fadeIn,
    //     durationInMilliseconds: 500,
    //   ),
    // ]
    )
class $RootRouter {}

// ignore: non_constant_identifier_names
final AppRouter = RootRouter();

/// Позволяет работать как-то так
/// ```dart
///   void clickAddButton(Widget child) {
///     chooseBottomSheet(appContext, child: child);
///   }
/// ```
final appContext = AppRouter.navigatorKey.currentContext!;
