import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tablets_box/common/app_icons.dart';
import 'package:tablets_box/common/app_text_styles.dart';
import 'package:tablets_box/common/riverpod_widget.dart';
import 'package:tablets_box/model/data/medicine/medicine.dart';
import 'package:tablets_box/model/data/repository/repository.dart';
import 'package:tablets_box/model/data/schedule/schedule_action.dart';
import 'package:tablets_box/common/app_colors.dart';
import 'package:tablets_box/ui/screens/tablets_screen/schedule_screen.dart';

class ScheduleItemWidget extends RiverpodWidget<ScheduleItemWidgetController> {
  ScheduleItemWidget(
      {Key? key,
      required ScheduledAction scheduledAction,
      required ScheduleScreenController screenController})
      : super(
            key: key,
            builder: () => ScheduleItemWidgetController(
                schedule: scheduledAction, controller: screenController));

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final schedule = ref.watch(controller.scheduleProvider);

    if (controller.medicine == null) {
      return Container(
          margin: EdgeInsets.all(3.r),
          padding: EdgeInsets.all(5.r),
          decoration: BoxDecoration(
            color: AppColors.background,
            boxShadow: [
              BoxShadow(
                  color: AppColors.itemShadow,
                  blurRadius: 5,
                  offset: const Offset(0, 3),
                  spreadRadius: 2)
            ],
            borderRadius: const BorderRadius.all(Radius.circular(10)),
          ),
          child: Text(
            'Pending',
            style: AppTextStyles.text20,
          ));
    }
    final medicine = controller.medicine!;
    return Container(
        margin: EdgeInsets.all(3.r),
        padding: EdgeInsets.all(5.r),
        decoration: BoxDecoration(
          color: AppColors.background,
          boxShadow: [
            BoxShadow(
                color: AppColors.itemShadow,
                blurRadius: 5,
                offset: const Offset(0, 3),
                spreadRadius: 2)
          ],
          borderRadius: const BorderRadius.all(Radius.circular(10)),
        ),
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: controller.onItemTap,
          child: Row(
            children: [
              Image.asset(medicine.form.defaultIconPath),
              Column(
                children: [
                  Text(
                    medicine.name,
                    style: AppTextStyles.text14.andColor(AppColors.plainText),
                  ),
                  GestureDetector(
                    onTap: controller.onDeleteTap,
                    child: Image.asset(AppIcons.trash,
                        color: AppColors.blue, width: 15.r),
                  )
                ],
              )
            ],
          ),
        ));
  }
}

class ScheduleItemWidgetController
    extends RiverpodController<ScheduleItemWidget> {
  ScheduleItemWidgetController(
      {required ScheduledAction schedule, required this.controller}) {
    scheduleProvider = StateNotifierProvider((ref) {
      _ref = ref;
      return ScheduleNotifier(schedule);
    });
    _setMedicine(schedule);
  }
  final ScheduleScreenController controller;
  Medicine? get medicine => _medicine;
  late final StateNotifierProvider<ScheduleNotifier, ScheduledAction>
      scheduleProvider;
  late final StateNotifierProviderRef _ref;
  Medicine? _medicine;
  void _setMedicine(ScheduledAction schedule) async {
    final result = await Repository.getMedicine(schedule.medicineId);
    _medicine = result.fold((l) => null, (r) => r);
  }

  void onItemTap() {
    controller.onScheduleTap();
  }

  void onDeleteTap() {
    controller.onDeleteSchedule();
  }
}

class ScheduleNotifier extends StateNotifier<ScheduledAction> {
  ScheduleNotifier(ScheduledAction schedule) : super(schedule);
  set schedule(ScheduledAction schedule) => state = schedule;
  ScheduledAction get schedule => state;
}
