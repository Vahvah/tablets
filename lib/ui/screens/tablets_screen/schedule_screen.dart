import 'package:date_picker_timeline/date_picker_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tablets_box/common/app_extensions.dart';
import 'package:tablets_box/common/app_text_styles.dart';
import 'package:tablets_box/common/riverpod_widget.dart';
import 'package:tablets_box/model/data/schedule/schedule_action.dart';
import 'package:tablets_box/model/language/language.dart';

class ScheduleScreen extends RiverpodWidget<ScheduleScreenController> {
  ScheduleScreen({Key? key})
      : super(key: key, builder: () => ScheduleScreenController());

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final schedule = ref.watch(controller.scheduleProvider);
    final language = ref.watch(languageProvider);
    return Container(
      padding: EdgeInsets.all(5.r),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(controller.today),
            ],
          ),
          DatePicker(
            controller.getOldestSchedule(),
            height: 50.h,
            monthTextStyle: AppTextStyles.text10,
            dayTextStyle: AppTextStyles.text10,
            dateTextStyle: AppTextStyles.text12,
            initialSelectedDate: DateTime.now(),
            locale: language.code,
            onDateChange: controller.onDateSelected,
          ),
        ],
      ),
    );
  }
}

class ScheduleScreenController extends RiverpodController<ScheduleScreen> {
  ScheduleScreenController() {
    scheduleProvider = StateNotifierProvider((ref) {
      _ref = ref;
      return ScheduleNotifier(List.empty());
    });
  }
  late final StateNotifierProviderRef _ref;
  late final StateNotifierProvider<ScheduleNotifier, List<ScheduledAction>>
      scheduleProvider;

  String get today {
    final locale =
        _ref.read(languageProvider.notifier).currentLocal.languageCode;
    final today = DateFormat('dMMMM', locale).format(DateTime.now());
    return 'Сегодня: $today'; //TODO:Localization
  }

  DateTime getOldestSchedule() {
    return DateTime.now();
  }

  void onDateSelected(DateTime date) {}

  void onScheduleTap() {}

  void onDeleteSchedule() {}
}

class ScheduleNotifier extends StateNotifier<List<ScheduledAction>> {
  ScheduleNotifier(List<ScheduledAction> schedule) : super(schedule);
}
