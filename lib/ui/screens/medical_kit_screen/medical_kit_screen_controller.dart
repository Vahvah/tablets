import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tablets_box/common/riverpod_widget.dart';
import 'package:tablets_box/model/data/medicine/medicine.dart';
import 'package:tablets_box/model/data/repository/repository.dart';
import 'package:tablets_box/ui/router/root_router.dart';
import 'package:tablets_box/ui/router/root_router.gr.dart';
import 'package:tablets_box/ui/screens/medical_kit_screen/medical_kit_screen.dart';

class MedicalKitScreenController extends RiverpodController<MedicalKitScreen> {
  MedicalKitScreenController() {
    medicinesListProvider = StateNotifierProvider(
      (ref) {
        _ref = ref;
        final notifier = MedicinesNotifier(List.empty());
        refillMedicines();
        return notifier;
      },
    );
  }
  MedicalKitTab _currentTab = MedicalKitTab.values[0];
  late final StateNotifierProviderRef _ref;
  late final StateNotifierProvider<MedicinesNotifier, List<Medicine>>
      medicinesListProvider;
  void onTapMedicineItem(Medicine medicine) {}

  void onTapAddMedicine() {
    AppRouter.push(EditMedicineRoute(kitController: this, medicine: null));
  }

  void onAddMedicine(Medicine medicine) async {
    await Repository.addMedicine(medicine);
    refillMedicines();
  }

  void onSaveMedicine(Medicine medicine) async {
    await Repository.updateMedicine(medicine);
    refillMedicines();
  }

  void onMedicineItemTap(Medicine medicine) {}

  void onMedicineItemInKit(Medicine medicine) async {
    await Repository.updateMedicine(medicine);
    refillMedicines();
  }

  void onMedicineItemSchedule(Medicine medicine) async {
    await Repository.updateMedicine(medicine);
    refillMedicines();
  }

  void onMedicineDelete(Medicine medicine) async {
    await Repository.deleteMedicine(medicine);
    refillMedicines();
  }

  void refillMedicines() async {
    final list = await Repository.getMedicines();
    final provider = _ref.read(medicinesListProvider.notifier);
    final medicines = <Medicine>[];
    switch (_currentTab) {
      case MedicalKitTab.myMedicines:
        for (Medicine m in list) {
          if (m.isInKit) {
            medicines.add(m);
          }
        }
        break;
      case MedicalKitTab.allMedicines:
        medicines.addAll(list);
        break;
    }
    provider.setState(medicines);
  }

  void setCurrentTabView(MedicalKitTab tab) {
    _currentTab = tab;
    refillMedicines();
  }
}

class MedicinesNotifier extends StateNotifier<List<Medicine>> {
  MedicinesNotifier(List<Medicine> list) : super(list);
  void setState(List<Medicine> list) {
    state = list;
  }
}
