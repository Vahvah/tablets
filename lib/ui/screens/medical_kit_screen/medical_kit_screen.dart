import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tablets_box/common/riverpod_widget.dart';
import 'package:tablets_box/ui/screens/medical_kit_screen/medical_kit_screen_controller.dart';
import 'package:tablets_box/ui/screens/medical_kit_screen/widgets/all_medicines_list_widget.dart';
import 'package:tablets_box/ui/screens/medical_kit_screen/widgets/my_medicines_list_widget.dart';

enum MedicalKitTab { myMedicines, allMedicines }

class MedicalKitScreen extends RiverpodWidget<MedicalKitScreenController> {
  MedicalKitScreen({super.key})
      : super(builder: () => MedicalKitScreenController());

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return DefaultTabController(
        length: MedicalKitTab.values.length,
        child: Column(
          children: [
            TabBar(
                onTap: (tabNum) =>
                    controller.setCurrentTabView(MedicalKitTab.values[tabNum]),
                tabs: const [
                  Tab(
                    child: Text(
                      'Моя аптечка',
                      style: TextStyle(color: Colors.black),
                    ),
                  ), //TODO:Localization
                  Tab(
                    child: Text(
                      'Мои лекарства',
                      style: TextStyle(color: Colors.black),
                    ),
                  ), //TODO:Localization
                ]),
            Expanded(
              child: TabBarView(
                children: [
                  MyMedicinesListWidget(controller: controller),
                  AllMedicinesListWidget(controller: controller)
                ],
              ),
            ),
          ],
        ));
  }
}
