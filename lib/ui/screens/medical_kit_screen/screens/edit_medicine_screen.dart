import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tablets_box/common/app_colors.dart';
import 'package:tablets_box/common/app_extensions.dart';
import 'package:tablets_box/common/app_text_styles.dart';
import 'package:tablets_box/common/riverpod_widget.dart';
import 'package:tablets_box/common/widgets/app_editable_text_field.dart';
import 'package:tablets_box/model/data/medicine/medicine.dart';
import 'package:tablets_box/ui/router/root_router.dart';
import 'package:tablets_box/ui/screens/medical_kit_screen/medical_kit_screen_controller.dart';
import 'package:uuid/uuid.dart';

class EditMedicineScreen extends RiverpodWidget<EditMedicineScreenController> {
  EditMedicineScreen(
      {super.key,
      Medicine? medicine,
      required MedicalKitScreenController kitController})
      : super(
            builder: () => EditMedicineScreenController(
                controller: kitController, medicine: medicine));

//  final EditMedicineScreenController controller;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final medicineForm = ref.watch(controller.medicineFormProvider);
    final scheduled = ref.watch(controller.scheduledProvider);
    final isInMedicalKit = ref.watch(controller.isInKitProvider);
    return Scaffold(
      appBar: _appBar(),
      body: Container(
        margin: EdgeInsets.all(5.r),
        color: AppColors.background,
        child: Column(
          children: [
            AppEditableTextField(
              textController: controller.medicineNameController,
              hintText: 'Введите название препарата', //TODO:Localization
              textAlign: TextAlign.center,
            ),
            10.sbHeight0,
            Row(
              children: [
                Text(
                  'Лекарственная форма:', //TODO:Localization
                  style: AppTextStyles.text14
                      .andColor(AppColors.plainText)
                      .andWeight(FontWeight.w400),
                ),
                5.sbWidth0,
                DropdownButton<MedicineForm>(
                    alignment: Alignment.center,
                    value: medicineForm,
                    items: List.generate(MedicineForm.values.length, (index) {
                      return DropdownMenuItem<MedicineForm>(
                        value: MedicineForm.values[index],
                        child: Center(
                          child: Text(
                              MedicineForm.values[index].getLocalizedName,
                              style: AppTextStyles.text12
                                  .andColor(AppColors.plainText)),
                        ),
                      );
                    }),
                    onChanged: (form) =>
                        controller.onMedicineFormChanged(form!, ref)),
                const Spacer(),
                Image.asset(medicineForm.defaultIconPath),
              ],
            ),
            10.sbHeight,
            Container(
              padding: EdgeInsets.all(2.r),
              height: 100.h,
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        blurRadius: 5,
                        offset: const Offset(0, 3),
                        spreadRadius: 2)
                  ],
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(10.r)),
                  border: Border.all(color: Colors.grey)),
              child: TextField(
                textInputAction: TextInputAction.newline,
                keyboardType: TextInputType.multiline,
                maxLines: null,
                controller: controller.medicineDescriptionController,
                decoration: InputDecoration.collapsed(
                  hintText: 'Введите описание препарата', //TODO:Localization
                  hintStyle: AppTextStyles.text12.andColor(AppColors.hintText),
                ),
                style: AppTextStyles.text14.andColor(AppColors.plainText),
              ),
            ),
            5.sbHeight0,
            Row(
              children: [
                Checkbox(
                  value: isInMedicalKit,
                  onChanged: (isInMedicalKit) =>
                      controller.onIsInKitChange(ref, isInMedicalKit ?? false),
                ),
                Text(
                  'В аптечку', //TODO:Localization
                  style: AppTextStyles.text14
                      .andColor(AppColors.plainText)
                      .andWeight(FontWeight.w400),
                ),
              ],
            ),
            5.sbHeight0,
            Row(
              children: [
                Checkbox(
                  value: scheduled,
                  onChanged: (scheduled) =>
                      controller.onScheduledChange(ref, scheduled ?? false),
                ),
                Text(
                  'Запланировать', //TODO:Localization
                  style: AppTextStyles.text14
                      .andColor(AppColors.plainText)
                      .andWeight(FontWeight.w400),
                ),
              ],
            ),
            const Spacer(),
            Row(
              children: [
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        backgroundColor: AppColors.cancelColor,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.r))),
                        fixedSize: Size(150.w, 25.h)),
                    onPressed: controller.onCancel,
                    child: Text(
                      'Отмена',
                      style: AppTextStyles.text16.andColor(AppColors.plainText),
                    )),
                const Spacer(),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        backgroundColor: AppColors.applyColor,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.r))),
                        fixedSize: Size(150.w, 25.h)),
                    onPressed: () => controller.onApply(ref),
                    child: Text(
                      'Принять',
                      style:
                          AppTextStyles.text16.andColor(AppColors.contrastText),
                    )),
              ],
            )
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          height: MediaQuery.of(context).viewInsets.bottom,
        ),
      ),
    );
  }

  PreferredSizeWidget _appBar() {
    return AppBar(
      automaticallyImplyLeading: true,
      elevation: 0,
      toolbarHeight: 0,
      backgroundColor: AppColors.background,
      flexibleSpace: Container(),
    );
  }
}

class EditMedicineScreenController
    extends RiverpodController<EditMedicineScreen> {
  EditMedicineScreenController({required this.controller, this.medicine}) {
    medicineNameController = TextEditingController(
      text: medicine?.name ?? '',
    );
    medicineDescriptionController = TextEditingController(
      text: medicine?.description ?? '',
    );
    final form = medicine?.form ?? MedicineForm.tablet;
    medicineFormProvider =
        StateNotifierProvider<MedicineFormNotifier, MedicineForm>(
            (ref) => MedicineFormNotifier(form));
    final scheduled = medicine?.scheduled ?? false;
    scheduledProvider = StateNotifierProvider<ScheduledNotifier, bool>(
        (ref) => ScheduledNotifier(scheduled));
    isInKitProvider = StateNotifierProvider<IsInKitNotifier, bool>(
        (ref) => IsInKitNotifier(medicine?.isInKit ?? false));
  }
  late final StateNotifierProvider<MedicineFormNotifier, MedicineForm>
      medicineFormProvider;
  late final StateNotifierProvider<ScheduledNotifier, bool> scheduledProvider;
  late final StateNotifierProvider<IsInKitNotifier, bool> isInKitProvider;
  late final TextEditingController medicineNameController;
  late final TextEditingController medicineDescriptionController;
  final Medicine? medicine;
  final MedicalKitScreenController controller;

  void onMedicineFormChanged(MedicineForm newForm, WidgetRef ref) {
    ref.read(medicineFormProvider.notifier).setCurrentForm(newForm);
  }

  void onScheduledChange(WidgetRef ref, bool scheduled) {
    ref.read(scheduledProvider.notifier).setSchedule(scheduled);
  }

  void onIsInKitChange(WidgetRef ref, bool isInKit) {
    ref.read(isInKitProvider.notifier).setIsInKitNotifier(isInKit);
  }

  void onCancel() {
    AppRouter.pop();
  }

  void onApply(WidgetRef ref) {
    final newMedicine = Medicine(
      name: medicineNameController.value.text,
      id: medicine?.id ?? const Uuid().v4(),
      scheduled: ref.read(scheduledProvider.notifier).isScheduled,
      description: medicineDescriptionController.value.text,
      form: ref.read(medicineFormProvider.notifier).getState(),
      isInKit: ref.read(isInKitProvider.notifier).isInKitNotifier,
    );

    if (medicine == null) {
      controller.onAddMedicine(newMedicine);
    } else {
      controller.onSaveMedicine(newMedicine);
    }
    AppRouter.pop();
  }
}

class MedicineFormNotifier extends StateNotifier<MedicineForm> {
  MedicineFormNotifier(MedicineForm initial) : super(initial);
  void setCurrentForm(MedicineForm form) {
    state = form;
  }

  MedicineForm getState() => state;
}

class ScheduledNotifier extends StateNotifier<bool> {
  ScheduledNotifier(bool initial) : super(initial);
  void setSchedule(bool flag) => state = flag;
  void changeScheduled() => state = !state;
  bool get isScheduled => state;
}

class IsInKitNotifier extends StateNotifier<bool> {
  IsInKitNotifier(bool initial) : super(initial);
  void setIsInKitNotifier(bool flag) => state = flag;
  void changeIsInKitNotifier() => state = !state;
  bool get isInKitNotifier => state;
}
