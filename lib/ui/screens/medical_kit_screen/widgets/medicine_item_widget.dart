import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tablets_box/common/app_colors.dart';
import 'package:tablets_box/common/app_extensions.dart';
import 'package:tablets_box/common/app_icons.dart';
import 'package:tablets_box/common/app_text_styles.dart';
import 'package:tablets_box/common/riverpod_widget.dart';
import 'package:tablets_box/model/data/medicine/medicine.dart';
import 'package:tablets_box/ui/screens/medical_kit_screen/medical_kit_screen_controller.dart';

class MedicineItemWidget extends RiverpodWidget<MedicineItemWidgetController> {
  MedicineItemWidget(
      {Key? key,
      required Medicine medicine,
      required MedicalKitScreenController medicalKitScreenController})
      : super(
            key: key,
            builder: () => MedicineItemWidgetController(
                medicine, medicalKitScreenController));

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final medicine = ref.watch(controller.medicineProvider);
    return Container(
      margin: EdgeInsets.all(3.r),
      padding: EdgeInsets.all(5.r),
      decoration: BoxDecoration(
        color: AppColors.background,
        boxShadow: [
          BoxShadow(
              color: AppColors.itemShadow,
              blurRadius: 5,
              offset: const Offset(0, 3),
              spreadRadius: 2)
        ],
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: controller.onMedicineItemTap,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Row(
                    children: [
                      GestureDetector(
                          onTap: controller.onMedicineItemInKit,
                          child: Image.asset(AppIcons.medicalCross,
                              width: 15.r,
                              color: medicine.isInKit
                                  ? null
                                  : AppColors.disabled)),
                      5.sbHeight0,
                      GestureDetector(
                        onTap: controller.onMedicineItemTimer,
                        child: Icon(Icons.alarm,
                            color: medicine.scheduled
                                ? AppColors.blue
                                : AppColors.disabled,
                            size: 15.r),
                      ),
                      5.sbWidth0,
                      Text(
                        medicine.name,
                        style:
                            AppTextStyles.text14.andColor(AppColors.plainText),
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      medicine.description,
                      style: AppTextStyles.text12.andColor(AppColors.plainText),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                )
              ],
            ),
            const Spacer(),
            Image.asset(medicine.form.defaultIconPath),
            GestureDetector(
              onTap: controller.onMedicalItemDelete,
              child: Image.asset(AppIcons.trash,
                  color: AppColors.blue, width: 15.r),
            )
          ],
        ),
      ),
    );
  }
}

class MedicineItemWidgetController
    extends RiverpodController<MedicineItemWidget> {
  MedicineItemWidgetController(Medicine medicine, this.controller) {
    medicineProvider =
        StateNotifierProvider<MedicineStateNotifier, Medicine>((ref) {
      _ref = ref;
      return MedicineStateNotifier(medicine);
    });
  }

  late final StateNotifierProvider<MedicineStateNotifier, Medicine>
      medicineProvider;
  late final StateNotifierProviderRef _ref;
  final MedicalKitScreenController controller;

  void onMedicineItemTap() {
    final medicine = _ref.read(medicineProvider.notifier).medicine;
    controller.onMedicineItemTap(medicine);
  }

  void onMedicineItemInKit() {
    final medicine = _ref.read(medicineProvider.notifier).changeInKit();
    controller.onMedicineItemInKit(medicine);
  }

  void onMedicineItemTimer() {
    final medicine = _ref.read(medicineProvider.notifier).changeScheduled();
    controller.onMedicineItemSchedule(medicine);
  }

  void onMedicalItemDelete() {
    final medicine = _ref.read(medicineProvider.notifier).medicine;
    controller.onMedicineDelete(medicine);
  }
}

class MedicineStateNotifier extends StateNotifier<Medicine> {
  MedicineStateNotifier(Medicine medicine) : super(medicine);
  Medicine get medicine => state;
  Medicine changeInKit() {
    state = state.copyWith(isInKit: !state.isInKit);
    return state;
  }

  Medicine changeScheduled() {
    state = state.copyWith(scheduled: !state.scheduled);
    return state;
  }
}
