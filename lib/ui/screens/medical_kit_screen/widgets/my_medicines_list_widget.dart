import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tablets_box/common/app_extensions.dart';
import 'package:tablets_box/ui/screens/medical_kit_screen/medical_kit_screen_controller.dart';
import 'package:tablets_box/ui/screens/medical_kit_screen/widgets/medicine_item_widget.dart';

class MyMedicinesListWidget extends ConsumerWidget {
  const MyMedicinesListWidget({super.key, required this.controller});
  final MedicalKitScreenController controller;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final myMedicines = ref.watch(controller.medicinesListProvider);
    return Column(
      children: [
        ListView.separated(
          padding: const EdgeInsets.all(8),
          shrinkWrap: true,
          itemBuilder: (BuildContext context, int index) {
            return MedicineItemWidget(
                medicalKitScreenController: controller,
                medicine: myMedicines[index]);
          },
          separatorBuilder: (BuildContext context, int index) {
            return 5.sbHeight;
          },
          itemCount: myMedicines.length,
        ),
      ],
    );
  }
}
