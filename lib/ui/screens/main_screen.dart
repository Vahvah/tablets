import 'package:flutter/cupertino.dart';
import 'package:tablets_box/common/app_scaffold.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return AppScafold();
  }
}
