import 'package:freezed_annotation/freezed_annotation.dart';

part 'app_error.freezed.dart';

enum ErrorCode {
  notFound(404),
  exception(520);

  const ErrorCode(this.code);
  final int code;
}

@freezed
class AppError with _$AppError {
  factory AppError.notFound({@Default(ErrorCode.notFound) ErrorCode code}) =
      _NotFoundError;
  factory AppError.exception(
      {@Default(ErrorCode.notFound) ErrorCode code,
      required Exception exception}) = _exceptionError;
}
